export class Stock{
    // class properties
    symbol: string = "BBB"
    name: string
    date: string
    isEnabled: boolean
    price: number
    quantity: number
    // optional constructor
    constructor(){
    }
}