import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-symbol-form',
  templateUrl: './symbol-form.component.html',
  styleUrls: ['./symbol-form.component.css']
})
export class SymbolFormComponent implements OnInit {

  constructor() { }
  symbol = "GOOC"
  qty = "1000"
  combined

  ngOnInit() {
    this.combined = `${this.symbol} ${this.qty}`
  }

}
