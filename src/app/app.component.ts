import { Component, OnInit } from '@angular/core';
import { Stock } from './models/stock';
import { StockSymbolService } from './stock-symbol.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit{
  title = 'Angular';
  oneStock: Stock = new Stock();
  portfolio = [{
    symbol:'zzz', price: 250.75, qty:1200
  },{
    symbol:'xxx', price: 100.20, qty:1200
  },{
    symbol:'yyy', price: 230.00, qty:1200
  }];
  picURL = 'https://placehold.it/120x60';
  editing = '';
  symbolsModel;
  // methods of this class
  editEventHandler(data){
    this.editing = data
  }

  constructor(private stockSymbolService:StockSymbolService){

  }
  // make an initial call to the symbol service
  ngOnInit(){
    this.stockSymbolService.getSymbols()
      .subscribe((results)=>{
        this.symbolsModel = results;
      });
  }
}

